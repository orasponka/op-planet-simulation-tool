import pygame
import math
pygame.init()

width, height = 900, 900
fps = 120
window = pygame.display.set_mode((width, height))
pygame.display.set_caption("Planet Simulation")

white = (255, 255, 255)
black = (0,0,0)
yellow = (255, 255, 0)
blue = (100, 149, 237)
red = (188, 39, 50)
dark_grey = (80, 78, 81)
orangered = (255,69,0)
grey = (80, 80, 80)
lightbrown = (207, 158, 23)
lightblue = (135, 178, 232)
orange = (247, 155, 25)

default_font = pygame.font.SysFont(pygame.font.get_default_font(), 14)
hud_font = pygame.font.SysFont(pygame.font.get_default_font(), 23)

limit_orbit_size = True
save_orbit = True
lock_sun = True
show_asteroid_orbits = False
show_distance = False
show_name = True
show_astroids = True

day_multiplier = 5

scale_presets = [6.2, 11.4, 14.7, 23, 45, 84, 102, 280, 425, 560]
scale_names = ["The Kuiper Belt", "Pluto", "Neptune", "Uranus", "Saturn", "Jupiter", "Main Asteroid Belt", "Mars", "Earth", "Venus"]
scale_index = 0

class Planet:
    au = 149.6e6 * 1000
    g = 6.67428e-11
    scale = scale_presets[scale_index] / au
    timestep = 3600*24*day_multiplier

    def __init__(self, pos, radius, color, mass, turnaround_time, name):
        self.x = pos[0]
        self.y = pos[1]
        self.radius = radius
        self.color = color
        self.mass = mass

        self.orbit = []
        self.save_orbit = save_orbit
        self.sun = False
        self.distance_to_sun = 0
        self.turaround_time = turnaround_time

        self.x_vel = 0
        self.y_vel = 0

        self.name = name

    def draw(self, window):
        x = self.x * self.scale + width / 2
        y = self.y * self.scale + height / 2

        if len(self.orbit) > 2:
            updated_points = []
            for point in self.orbit:
                px, py = point
                px = px * self.scale + width / 2
                py = py * self.scale + height / 2
                updated_points.append([px, py])

            pygame.draw.lines(window, self.color, False, updated_points, 2)

        pygame.draw.circle(window, self.color, (x, y), self.radius)

        if not self.sun:
            if show_distance:
                distance_text = default_font.render(f"{round(self.distance_to_sun/1000, 1)}km", 1, white)
                window.blit(distance_text, (x - distance_text.get_width() / 2, y + distance_text.get_height()))
            if show_name:
                name_text = default_font.render(str(self.name), 1, white)
                window.blit(name_text, (x - name_text.get_width() / 2, y - name_text.get_height() - 5))

    def attraction(self, other):
        other_x, other_y = other.x, other.y
        distance_x = other_x - self.x
        distance_y = other_y - self.y
        distance = math.sqrt(distance_x ** 2 + distance_y ** 2)

        if other.sun:
            self.distance_to_sun = distance


        force = self.g * self.mass * other.mass / distance**2
        theta = math.atan2(distance_y, distance_x)
        force_x = math.cos(theta) * force
        force_y = math.sin(theta) * force
        return force_x, force_y
    def update_pos(self, planets):
        if self.sun and lock_sun:
            return

        total_fx = total_fy = 0

        for p in planets:
            if self == p:
                continue

            fx, fy = self.attraction(p)
            total_fx += fx
            total_fy += fy

        self.x_vel += total_fx / self.mass * self.timestep
        self.y_vel += total_fy / self.mass * self.timestep

        self.x += self.x_vel * self.timestep
        self.y += self.y_vel * self.timestep
        if len(self.orbit) > self.turaround_time//day_multiplier and self.save_orbit and limit_orbit_size:
            self.save_orbit = False
        if not self.sun and self.save_orbit:
            self.orbit.append((self.x, self.y))

def update_window(planets, pause):
    window.fill(black)

    for p in planets:
        if not pause:
            p.update_pos(planets)
        if not show_astroids and p.name in ["5145 Pholus","2 Pallas", "52 Europa", "10 Hygiea", "704 Interamnia","4 Vesta", "2060 Chiron", "99942 Apophis","944 Hildago","3 Juno"]:
            continue
        else:
            p.draw(window)

    if pause:
        pause_text = hud_font.render("Paused", 1, white)
        window.blit(pause_text, (5, 5))

    zoom_text = hud_font.render("Zoom: " + scale_names[scale_index], 1, white)
    window.blit(zoom_text, (5, height - zoom_text.get_height() - 5))

    timestep_text = hud_font.render("Timestep: " + str(day_multiplier) + " Days", 1, white)
    window.blit(timestep_text, (5, height - timestep_text.get_height() - zoom_text.get_height() - 10))

    pygame.display.update()

def main():
    global day_multiplier, show_astroids, show_distance, scale_index, save_orbit
    run = True
    pause = False
    restart = False
    clock = pygame.time.Clock()


    sun = Planet([0,0], 4, yellow, 1.98892 * 10**30, 0, "Sun")
    sun.sun = True

    earth = Planet([-1 * Planet.au, 0], 3, blue, 5.9742 * 10**24, 385, "Earth")
    earth.y_vel = 29.783 * 1000

    mars = Planet([-1.524 * Planet.au, 0], 3, red, 6.39 * 10**23, 696, "Mars")
    mars.y_vel = 24.077 * 1000

    mercury = Planet([0.387 * Planet.au, 0], 3, dark_grey, 3.30 * 10**23, 120, "Mercury")
    mercury.y_vel = -47.4 * 1000

    venus = Planet([0.723 * Planet.au, 0], 3, orange, 4.8685 * 10**24, 245, "Venus")
    venus.y_vel = -35.02 * 1000

    vesta = Planet([2.362 * Planet.au, 0], 3, grey, 2.59 * 10**20, 1325, "4 Vesta")
    vesta.y_vel = -19.34 * 1000

    pallas = Planet([2.772 * Planet.au, 0], 3, grey, 2.2 * 10**22, 1685,"2 Pallas")
    pallas.y_vel = -17.93 * 1000

    jupiter = Planet([5.203 * Planet.au, 0], 5, orangered, 1.899 * 10**27, 4330, "Jupiter")
    jupiter.y_vel = -13.06 * 1000

    saturn = Planet([9.582 * Planet.au, 0], 5, lightbrown, 5.688 * 10**26, 11200, "Saturn")
    saturn.y_vel = -9.68 * 1000

    uranus = Planet([19.19126393 * Planet.au, 0], 5, lightblue, 8.686 * 10**25, 30664, "Uranus")
    uranus.y_vel = -6.80 * 1000

    ceres = Planet([2.766 * Planet.au, 0], 3, grey, 9.445 * 10**20, 1680, "1 Ceres")
    ceres.y_vel = -17.882 * 1000

    europa = Planet([3.444 * Planet.au, 0], 3, grey, 24 +- 4 * 10**18, 1990, "52 Europa")
    europa.y_vel = -16.92 * 1000

    hygiea = Planet([2.77 * Planet.au, 0], 3, grey, 87.4 +- 6.9 * 10**18, 2032, "10 Hygiea")
    hygiea.y_vel = -16.76 * 1000

    interamnia = Planet([3.056 * Planet.au, 0], 3, grey, 35 +- 5 * 10**18, 1955, "704 Interamnia")
    interamnia.y_vel = -16.92 * 1000

    apophis = Planet([0.92 * Planet.au, 0], 3, grey, 6.1 * 10**10, 324, "99942 Apophis")
    apophis.y_vel = 30.73 * 1000

    chiron = Planet([13.7 * Planet.au, 0], 4, grey, 2.7 +- 0.3 * 10**18, 18523, "2060 Chiron")
    chiron.y_vel = -7.75 * 1000

    neptune = Planet([30.09 * Planet.au, 0], 4, blue, 1.024 * 10**26, 60149, "Neptune")
    neptune.y_vel = -5.43 * 1000

    pluto = Planet([39.5 * Planet.au, 0], 4, grey, 1.290 * 10**22, 90717, "Pluto")
    pluto.y_vel = -4.67 * 1000

    haumea = Planet([35.164 * Planet.au, 0], 4, grey, 4.2 +- 0.1 * 10**21, 104171, "Haumea")
    haumea.y_vel = -4.484 * 1000

    pholus = Planet([20.348 * Planet.au, 0], 4, grey, 6.6 * 10**18, 33526, "5145 Pholus")
    pholus.y_vel = -6.62 * 1000

    hildago = Planet([5.741 * Planet.au, 0], 4, grey, 8.4 * 10**15, 5024, "944 Hildago")
    hildago.y_vel = -12.44 * 1000

    orcus = Planet([-39.419 * Planet.au, 0], 4, grey, 7.5 * 10**22, 90335, "90482 Orcus")
    orcus.y_vel = 4.68 * 1000

    quaoar = Planet([-41.914 * Planet.au, 0], 4, grey, 1.4+-0.21 * 10**21, 105495, "50000 Quaoar")
    quaoar.y_vel = 4.52 * 1000

    juno = Planet([2.668 * Planet.au, 0], 3, grey, 2.86 * 10**19, 1592, "3 Juno")
    juno.y_vel = -17.93 * 1000

    makemake = Planet([45.43 * Planet.au, 0], 3, grey, 3.1 * 10**21, 111845, "136472 Makemake")
    makemake.y_vel = -4.419 * 1000

    eris = Planet([-67.864 * Planet.au, 0], 3, grey, 1.6 * 10**22, 204199, "136199 Eris")
    eris.y_vel = 3.434 * 1000

    if not show_asteroid_orbits:
        pallas.save_orbit = False
        vesta.save_orbit = False
        ceres.save_orbit = False
        europa.save_orbit = False
        hygiea.save_orbit = False
        interamnia.save_orbit = False
        apophis.save_orbit = False
        chiron.save_orbit = False
        pluto.save_orbit = False
        haumea.save_orbit = False
        pholus.save_orbit = False
        hildago.save_orbit = False
        orcus.save_orbit = False
        quaoar.save_orbit = False
        juno.save_orbit = False
        makemake.save_orbit = False
        eris.save_orbit = False

    planets = [sun, earth, mars, mercury, venus, vesta, pallas, jupiter, saturn, uranus, ceres, europa, juno, eris,
               hygiea, interamnia, apophis, chiron, neptune, pluto, haumea, pholus, hildago, orcus, quaoar, makemake]

    while run:
        clock.tick(fps)

        for e in pygame.event.get():
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE:
                    pygame.quit()
                if e.key == pygame.K_p:
                    if pause:
                        pause = False
                    elif not pause:
                        pause = True
                if e.key == pygame.K_LEFT:
                    if day_multiplier - 0.5 > 0:
                        day_multiplier -= 0.5
                        Planet.timestep = 3600*24*day_multiplier
                        restart = True
                        run = False
                if e.key == pygame.K_RIGHT:
                    if day_multiplier + 0.5 < 10.5:
                        day_multiplier += 0.5
                        Planet.timestep = 3600*24*day_multiplier
                        restart = True
                        run = False
                if e.key == pygame.K_a:
                    if show_astroids:
                        show_astroids = False
                    elif not show_astroids:
                        show_astroids = True
                if e.key == pygame.K_d:
                    if show_distance:
                        show_distance = False
                    elif not show_distance:
                        show_distance = True
                if e.key == pygame.K_UP:
                    if scale_index + 1 < len(scale_presets):
                        scale_index += 1
                        Planet.scale = scale_presets[scale_index] / Planet.au
                if e.key == pygame.K_DOWN:
                    if scale_index - 1 > -1:
                        scale_index -= 1
                        Planet.scale = scale_presets[scale_index] / Planet.au
                if e.key == pygame.K_o:
                    if save_orbit:
                        save_orbit = False
                    elif not save_orbit:
                        save_orbit = True
                    restart = True
                    run = False
            if e.type == pygame.QUIT:
                run = False
        if run:
            update_window(planets, pause)


    if not restart:
        pygame.quit()
    elif restart:
        main()

main()
